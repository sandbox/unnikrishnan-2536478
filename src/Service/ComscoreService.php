<?php
 
/**
 * @file
 * Contains Drupal\comscore_analytics\ComscoreService.
 */
 
namespace Drupal\comscore_analytics\Service;


class ComscoreService {

  //Total Population Based
  private $rfCensusOption = 1;
  private $rfReachOption = 1;

  private $soapClient;
  
  public function __construct($soapClient) {
    $this->soapClient = $soapClient;
  }

  /**
   * For finding values for different parameters in SOAP request.
   * 
   * @param $parameter_id
   *  Parameter for which values need to be identified
   * @param $params
   *  dependent parameters
   *
   * @return result
   *
   */
  public function discoverParameterValues($parameter_id, $params = []) {
    $data = [
      'parameterId' => $parameter_id,
    ];
    if(!empty($params)) {
      $data['query'] = array(
        'Parameter' => array(),
      );
      foreach ($params as $key => $value) {
        $data['query']['Parameter'][] = array('KeyId' => $key,  'Value' => $value);
      }
    }
    $result = $this->soapClient->__soapCall("DiscoverParameterValues", array($data));
    if(!empty($result)) {
      return $result->DiscoverParameterValuesResult->EnumValue;
    }
    return null;
  }

  /**
   * For finding all the media available in a geographical area.
   * 
   * @param $criteria
   *  search criteria for the media name
   */
  public function fetchMedia($criteria, $params = []) {
    $data = [
      'parameterId' => 'media',
      'fetchMediaQuery' => array(
        'xmlns' => 'http://comscore.com/FetchMedia',
        'SearchCritera' => array(
          array('Critera' => 'Platform'),
          array('Critera' => 'Advertising'),
          array("ExactMatch" => false, 'Critera' => $criteria),
        ),
      ),
      'reportQuery' => array(
        'xmlns' => 'http://comscore.com/FetchMedia',
        'Parameter' => array(
          array('KeyId' => 'geo',  'Value' => $params['geo']), 
          array('KeyId' => 'timeType',  'Value' => $params['timeType']),
          array('KeyId' => 'timePeriod',  'Value' => $params['timePeriod']),
          array('KeyId' => 'mediaSetType',  'Value' => $params['mediaSetType']),
        ),
      )    
    ];
    $result = $this->soapClient->__soapCall("FetchMedia", array($data));
    if(!empty($result)) {
      return $result->FetchMediaResult->MediaItem;
    }
  }

  /**
   * For finding media metrix.
   * 
   * @param $media
   *  media array for which we need to find metrics.
   * @param $params
   *  different dependency parameter for finding media report.
   */
  public function submitReport($media, $params = []) {
    $data = [
      'query' => array(
        'Parameter' => array(
          // Geography
          array('KeyId' => 'geo', 'Value' => $params['geo']), 
          // For all locations.
          array('KeyId' => 'loc', 'Value' => $params['loc']), 
          array('KeyId' => 'timePeriod', 'Value' => $params['timePeriod']), 
          array('KeyId' => 'targetGroup', 'Value' => $params['targetGroup']), 
          array('KeyId' => 'targetType', 'Value' => $params['targetType']), 
          array('KeyId' => 'rfCensusOption', 'Value' => $this->rfCensusOption),
          array('KeyId' => 'rfReachOption', 'Value' => $this->rfReachOption),
        ),
        'RFInput' => array(),
      )
    ];
    foreach($media as $medium) {
      $data['query']['Parameter'][] = array(
        'KeyId' => 'media',  
        'Value' => $medium['comscore_id'],
      );
      $data['query']['RFInput'][] = array(
        "MediaId" => $medium['comscore_id'],
        "TargetId" => $params['targetId'], 
        "Duration" => $medium['duration'],
        "Impressions" => $medium['impressions'],
        "FrequencyCap" => $medium['frequency_cap'], 
        "ReachFactor" => $medium['reach_factor'], 
        "CPM" => $medium['cpm'],
      );
    }
    $data['query']['Parameter'][] = array('KeyId' => 'target', 'Value' =>  $params['ageGroup']);
    $result = $this->soapClient->__soapCall("SubmitReport", array($data));
    if(!empty($result)) {
      return $result->SubmitReportResult;
    }
  }

  /**
   * For fetching report from comScore.
   * 
   * @param $jobid
   *  jobid for which report need to be fetched.
   */
  public function fetchReport($jobid) {
    $data = [
      'jobId' => $jobid,
    ];
    $result = $this->soapClient->__soapCall("FetchReport", array($data));
    if(!empty($result)) {
      return $result->FetchReportResult;
    }
  }

  /**
   * For checking the job status in comScore.
   * 
   * @param $jobid
   *   jobid for which status need to checked.
   */
  public function ping_job_status($jobid) {
    $data = [
      'jobId' => $jobid,
    ];
    $result = $this->soapClient->__soapCall("PingReportStatus", array($data));
    if(!empty($result)) {
      return $result->PingReportStatusResult->Status;
    }
  }

}
