<?php
 
/**
 * @file
 * Contains Drupal\comscore_analytics\Service\ComscoreFactory.
 */
 
namespace Drupal\comscore_analytics\Service;

use SoapClient;

class ComscoreFactory {
  /**
   * Create a singleton instance for ComscoreService.
   */
  public static function createInstance() {
    static $instance = null;
    if($instance === null) {
      //for getting comScore credentials.
      $config = \Drupal::config('comscore_analytics.settings');
      $login = $config->get('comscore_analytics_account');
      $password = $config->get('comscore_analytics_password');
      $reach_frequency_endpoint = \Drupal::config('comscore_analytics.settings')->get('comscore_endpoint_reach_frequency');
      $soap_client = new SoapClient($reach_frequency_endpoint, array(
        'login' => $login,
        'password' => $password,
      ));
      $instance = new ComscoreService($soap_client);
    }
    return $instance;
  }
  
  public function __construct() {
  }
}
