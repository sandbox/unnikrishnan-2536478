<?php
/**
 * @file
 * Contains \Drupal\comscore_analytics\Form\ComscoreSettingsForm.
 */

namespace Drupal\comscore_analytics\Form;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure comscore_analytics settings for this site.
 */
class ComscoreSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'comscore_analytics_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['comscore_analytics.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('comscore_analytics.settings');

    $form['general'] = [
      '#type' => 'details',
      '#title' => t('Account settings'),
      '#open' => TRUE,
    ];

    $form['general']['comscore_analytics_account'] = [
      '#default_value' => $config->get('comscore_analytics_account'),
      '#description' => t('Comscore account user name'),
      '#maxlength' => 100,
      '#required' => TRUE,
      '#size' => 50,
      '#title' => t('Account name'),
      '#type' => 'textfield',
    ];

    $form['general']['comscore_analytics_password'] = [
      '#default_value' => $config->get('comscore_analytics_password'),
      '#description' => t('Comscore account password'),
      '#maxlength' => 100,
      '#required' => TRUE,
      '#size' => 50,
      '#title' => t('Account password'),
      '#type' => 'textfield',
    ];

    $form['end_points'] = [
      '#type' => 'details',
      '#title' => t('Comscore API endpoints'),
      '#open' => TRUE,
    ];

    $default_value = $config->get('comscore_endpoint_reach_frequency') == '' ? 'https://api.comscore.com/ReachFrequency/CampaignRf.asmx?WSDL' : $config->get('comscore_endpoint_reach_frequency');
    $form['end_points']['comscore_endpoint_reach_frequency'] = [
      '#default_value' => $default_value,
      '#description' => t('Comscore end point for reach frequency SOAP request'),
      '#maxlength' => 500,
      '#required' => TRUE,
      '#size' => 100,
      '#title' => t('Reach Frequency'),
      '#type' => 'textfield',
    ];

    $default_value = $config->get('comscore_endpoint_key_measures') == '' ? 'https://api.comscore.com/KeyMeasures.asmx?WSDL' : $config->get('comscore_endpoint_key_measures');
    $form['end_points']['comscore_endpoint_key_measures'] = [
      '#default_value' => $default_value,
      '#description' => t('Comscore end point for key measures SOAP request'),
      '#maxlength' => 500,
      '#required' => TRUE,
      '#size' => 100,
      '#title' => t('KeyMeasures'),
      '#type' => 'textfield',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($form_state->getValue('comscore_endpoint_reach_frequency') == '') {
      $form_state->setErrorByName('comscore_endpoint_reach_frequency', t('Please specify the reach frequency end point'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('comscore_analytics.settings');
    $config
      ->set('comscore_analytics_account', $form_state->getValue('comscore_analytics_account'))
      ->set('comscore_analytics_password', $form_state->getValue('comscore_analytics_password'))
      ->set('comscore_endpoint_reach_frequency', trim($form_state->getValue('comscore_endpoint_reach_frequency')))
      ->set('comscore_endpoint_key_measures', trim($form_state->getValue('comscore_endpoint_key_measures')))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
