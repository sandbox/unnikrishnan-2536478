<?php

/**
 * @file
 * Contains \Drupal\comscore_analytics\Form\ComscoreSearchForm.
 * 
 * For searching a media metrix information in comScore.
 */

namespace Drupal\comscore_analytics\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\comscore_analytics\Ajax\ComscoreFetchReport;
/**
 * Search form for comscore data matrex.
 */
class ComscoreSearchForm extends FormBase {
 
  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'comscore_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $countries = $form_state->getBuildInfo()['args'][0];

    $form['wrapper'] = array(
      '#tree' => TRUE,
      '#prefix' => '<div id="comscore-wrap">',
      '#suffix' => '</div>'
    );

    $form['wrapper']['search'] = [
      '#type' => 'details',
      '#title' => t('Parameters'),
      '#open' => TRUE,
    ];

    $form['wrapper']['search']['duration'] = [
      '#default_value' => isset($_SESSION['comscore_duration']) ? $_SESSION['comscore_duration'] : 30,
      '#description' => t('Duration of the campaign'),
      '#required' => TRUE,
      '#title' => t('Duration'),
      '#type' => 'textfield',
      '#size' => 10,
    ];
    // Country
    $form['wrapper']['search']['country'] = [
      '#description' => t('Country'),
      '#required' => TRUE,
      '#title' => t('Country'),
      '#type' => 'select',
      '#options' => $countries,
      '#ajax' => array(
        // Function to call when event on form element triggered.
        'callback' => array($this, 'getTimePeriod'),
        // Effect when replacing content. Options: 'none' (default), 'slide', 'fade'.
        // Javascript event to trigger Ajax. Currently for: 'onchange'.
        'event' => 'change',
        'wrapper' => 'comscore-wrap',
        'progress' => array(
          // Graphic shown to indicate ajax. Options: 'throbber' (default), 'bar'.
          'type' => 'throbber',
          // Message to show along progress graphic. Default: 'Please wait...'.
          'message' => NULL,
        ),
      ),
    ];

    // Country
    $form['wrapper']['search']['time_period'] = [
      '#description' => t('Time Period'),
      '#required' => TRUE,
      '#title' => t('Time Period'),
      // TODO: should be fixed
      '#validated' => TRUE,
      '#states' => array(
        'invisible' => array(
          'select[name="wrapper[search][country]"]' => array('value' => ''),
        ),
      ),
      '#type' => 'select',
      '#options' => isset($_SESSION['time_periods']) ? $_SESSION['time_periods'] : array(),
      '#ajax' => array(
        // Function to call when event on form element triggered.
        'callback' => array($this, 'getAgeGroups'),
        // Effect when replacing content. Options: 'none' (default), 'slide', 'fade'.
        // Javascript event to trigger Ajax. Currently for: 'onchange'.
        'event' => 'change',
        'wrapper' => 'comscore-wrap',
        'progress' => array(
          // Graphic shown to indicate ajax. Options: 'throbber' (default), 'bar'.
          'type' => 'throbber',
          // Message to show along progress graphic. Default: 'Please wait...'.
          'message' => NULL,
        ),
      ),
    ];

    // Age groups
    $form['wrapper']['search']['age_group'] = [
      '#description' => t('Age group'),
      '#type' => 'select',
      '#states' => array(
        'invisible' => array(
          'select[name="wrapper[search][country]"]' => array('value' => ''),
        ),
      ),
      '#required' => TRUE,
      '#title' => t('Age group'),
      // TODO: should be fixed
      '#validated' => TRUE,
      '#options'=> isset($_SESSION['selected_age_grp']) ? $_SESSION['selected_age_grp'] : array(),
    ];

    // Impressions
    $form['wrapper']['search']['impressions'] = [
      '#default_value' => isset($_SESSION['comscore_impressions']) ? $_SESSION['comscore_impressions'] : 10,
      '#description' => t('impressions'),
      '#required' => TRUE,
      '#title' => t('Impressions'),
      '#type' => 'textfield',
      '#size' => 10,
    ];

    // CPM
    $form['wrapper']['search']['cpm'] = [
      '#description' => t('CPM'),
      '#maxlength' => 100,
      '#required' => TRUE,
      '#title' => t('CPM'),
      '#default_value' => isset($_SESSION['comscore_cpm']) ? $_SESSION['comscore_cpm'] : 0.1,
      '#type' => 'textfield',
      '#size' => 10,
    ];
    // Frequency cap
    $form['wrapper']['search']['frequency_cap'] = [
      '#description' => t('Frequency Cap'),
      '#maxlength' => 100,
      '#title' => t('Frequency Cap'),
      '#type' => 'textfield',
      '#size' => 10,
      '#default_value' => isset($_SESSION['comscore_frequency_cap']) ? $_SESSION['comscore_frequency_cap'] : 3,
    ];

    // Media
    $form['wrapper']['search']['media'] = [
      '#description' => t('media'),
      '#required' => TRUE,
      '#title' => t('Media'),
      '#states' => array(
        'invisible' => array(
          'select[name="wrapper[search][age_group]"]' => array('value' => ''),
        ),
      ),
      '#autocomplete_route_name' => 'comscore_analytics.media',
      '#type' => 'textfield',
      '#size' => 10,
    ];

    $form['wrapper']['submit'] = array(
      '#type' => 'submit', 
      '#value' => t('Search'),
      '#ajax' => array(
        'callback' => array($this, 'submitReport'),
        'event' => 'click',
        'progress' => array(
          'type' => 'throbber',
          'message' => 'Searching...',
       ),
      ),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $_SESSION['comscore_duration'] = $form_state->getValue(array('wrapper', 'search', 'duration'));
    $_SESSION['comscore_country'] = $form_state->getValue(array('wrapper', 'search', 'country'));
    $_SESSION['comscore_frequency_cap'] = $form_state->getValue(array('wrapper', 'search','frequency_cap'));
    $_SESSION['comscore_cpm'] = $form_state->getValue(array('wrapper', 'search', 'cpm'));
    $_SESSION['comscore_age_group'] = $form_state->getValue(array('wrapper', 'search', 'age_group'));
    $_SESSION['comscore_time_period'] = $form_state->getValue(array('wrapper', 'search', 'time_period'));

  }
  
  /**
   * submit report to comScore.
   */
  public function submitReport(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();
    $comscore = \Drupal\comscore_analytics\Service\ComscoreFactory::createInstance();
    $mediaName = $form_state->getValue(array('wrapper', 'search', 'media'));
    $comscore_id = array_search($mediaName, $_SESSION['selected_media']);
    $duration =  $form_state->getValue(array('wrapper', 'search', 'duration'));
    $media = [array(
      'comscore_id' => $comscore_id,
      'duration' => $duration, 
      'impressions' => $form_state->getValue(array('wrapper', 'search', 'impressions')),
      'frequency_cap' => $form_state->getValue(array('wrapper', 'search', 'frequency_cap')),
      'reach_factor' => 1,
      'cpm' => $form_state->getValue(array('wrapper', 'search', 'cpm')),
    )];

    $result = $comscore->submitReport($media, array(
      'geo' => $form_state->getValue(array('wrapper', 'search', 'country')), 
      'timeType' => 1,
      'loc' => 0,
      'mediaSetType' => 1,
      'timePeriod' => $form_state->getValue(array('wrapper', 'search', 'time_period')),
      'targetGroup' => 20,
      'targetType' => 0,
      'ageGroup' => $form_state->getValue(array('wrapper', 'search', 'age_group')),
      'targetId' => -1,

    )); 
    // If there is an error in the process.
    $error = "";
    if(isset($result->Errors)) {
      if(is_array($result->Errors)) {
        foreach ($result->Errors as $_error) {
          $error .= $_error->Message ."\n";
        }
      }
      else {
        $error = $result->Errors->Message;
      }
    }
    if($error != "") {
      $ajax_response->addCommand(new AlertCommand(t($error)));
    }
    else {
      $ajax_response->addCommand(new ComscoreFetchReport($result->JobId));
    }
    // Return the AjaxResponse Object.
    return $ajax_response;
  }

  /**
   * For getting the available time periods from comScore.
   *
   * @param $form
   *
   * @param $form_state
   *
   */
  public function getTimePeriod(array &$form, FormStateInterface &$form_state) {
    $geo = $form_state->getValue(array('wrapper', 'search', 'country'));
    $comscore = \Drupal\comscore_analytics\Service\ComscoreFactory::createInstance();
    $comscore_time_period = $comscore->discoverParameterValues('timePeriod', array(
      'geo' => $geo, 
      'timeType' => 1,
    )); 
    $time_periods = array();
    foreach ($comscore_time_period as $time_period) {
      $time_periods[$time_period->Id] = $time_period->Value;
    }
    $_SESSION['comscore_country'] = $form_state->getValue(array('wrapper', 'search', 'country'));
    $form['wrapper']['search']['time_period']['#options'] = $time_periods;
    return $form['wrapper']; 
  }

  /**
   * For getting availabale age groups in geographical area
   */
  public function getAgeGroups(array &$form, FormStateInterface &$form_state) {
    $form_state->setRebuild(TRUE);
    $geo = $form_state->getValue(array('wrapper', 'search', 'country'));
    $time_period = $form_state->getValue(array('wrapper', 'search', 'time_period'));
    $comscore = \Drupal\comscore_analytics\Service\ComscoreFactory::createInstance();
    $comscoreAgeGroups = $comscore->discoverParameterValues('target', array(
      'geo' => $geo, 
      'timeType' => 1,
      'timePeriod' => $time_period,
    ));
    $age_groups = array();
    foreach ($comscoreAgeGroups as $age_group) {
      $age_groups[$age_group->Id] = $age_group->Value;
    }
    $_SESSION['comscore_time_period'] = $form_state->getValue(array('wrapper', 'search', 'time_period'));
    $form['wrapper']['search']['age_group']['#options'] = $age_groups;
    return $form['wrapper'];
  }

  /**
   * For getting available media for campaign submit report
   */
  public function getAvailableMedia(Request $request) {
    $criteria = $request->query->get('q');
    $geo = $_SESSION['comscore_country'];
    $time_period = $_SESSION['comscore_time_period'];
    $comscore = \Drupal\comscore_analytics\Service\ComscoreFactory::createInstance();
    $comscoreMedia = $comscore->fetchMedia($criteria, array(
      'geo' => $geo, 
      'timeType' => 1,
      'timePeriod' => $time_period,
      'mediaSetType' => 1,
    ));
    $media = array();
    foreach ($comscoreMedia as $mediaum) {
      $media[$mediaum->Id] = $mediaum->Name;
    }
    $_SESSION['selected_media'] = $media;
    if(empty($media)) {
      $media[] = "No result";
    }
    return new JsonResponse(array_values($media));
  }
}