<?php
 
/**
 * @file
 * Contains \Drupal\comscore_analytics\Controller\ComscoreController.
 */

namespace Drupal\comscore_analytics\Controller;
 
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ChangedCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * ComscoreController.
 */
class ComscoreController extends ControllerBase {
  // for accessing comScore service 
  protected $comscoreService;
  // for templating
  protected $twig;

  /**
   * The form builder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;  

  /**
   * Class constructor.
   */
  public function __construct($comscoreService, TwigEnvironment $twig, FormBuilderInterface $form_builder) {
    $this->comscoreService = $comscoreService;
    $this->twig = $twig;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('comscore_analytics.comscore.service'),
      $container->get('twig'),
      $container->get('form_builder')
    );
  }

  /**
   * Generates comScore metrix data dashboard.
   */
  public function dashBoard() {
    if(!isset($_SESSION['avalibale_country'])) {
      $comscore_countries = $this->comscoreService->discoverParameterValues('geo');
      $countries = array();
      foreach ($comscore_countries as $country) {
        $countries[$country->Id] = $country->Value;
      }
      $_SESSION['avalibale_country'] = $countries;
    }
    else {
      $countries = $_SESSION['avalibale_country'];
    }
    //$form_object = new \Drupal\comscore_analytics\Form\ComscoreSearchForm();
    //$form_object->set_country($countries);
    $filter = $this->formBuilder->getForm("Drupal\comscore_analytics\Form\ComscoreSearchForm", $countries);
    return [
      'filter' => $filter,
      '#suffix' => "<div id='search-result'></div>",
      '#attached' => [ 
        'library' => [
          'comscore_analytics/comscore_dashboard',
        ],
      ]
    ];
  }
  
  /**
   * fetch report from comscore.
   */
  public function fetchReport(Request $request) {
    $jobid = $request->request->get('job_id');
    $job_status = $this->comscoreService->ping_job_status($jobid);
    if($job_status != 'Completed') {
      return new JsonResponse(array(
        'status' => true,
        'content' => $job_status,
      ));      
    }
    else {
      return new JsonResponse(array(
        'status' => true,
        'content' => $this->comscoreService->fetchReport($jobid),
      ));
    }
  }
}