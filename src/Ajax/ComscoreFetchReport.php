<?php

/**
 * @file
 * Contains \Drupal\comscore_analytics\Ajax\ComscoreFetchReport.php.
 */

namespace Drupal\comscore_analytics\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Provides an AJAX command for initiate fetching data from comScore.
 *
 * This command is implemented in comscore_analytics.js in
 * Drupal.AjaxCommands.prototype.comscoreFetchReport.
 */
class ComscoreFetchReport implements CommandInterface {

  /**
   * JobId in Comscore
   *
   * @var integer
   */
  protected $jobID;

  /**
   * Constructs a ComscoreFetchReport object.
   *
   * @param string $values
   *   The values that should be passed to the form constructor in Drupal.
   */
  public function __construct($values) {
    $this->jobID = $values;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return array(
      'command' => 'comscoreFetchReport',
      'values' => array($this->jobID),
    );
  }

}
