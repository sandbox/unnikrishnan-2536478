/**
 * @file
 * For checking the job status in comScore and fetching the report. 
 */

(function ($, Drupal) {
  Drupal.AjaxCommands.prototype.comscoreFetchReport = function (ajax, response, status) {
    fetch_report(response.values[0]);
  };
})(jQuery, Drupal);

/**
 * For getting report from comscore job
 */
function fetch_report(job_id) {
  (function($, Drupal, window) {
    $.post('fetch-report', {'job_id' : job_id}, function(data) {
      if (data.content == 'Queued' || data.content == 'Processing') {
        setTimeout(fetch_report(job_id), 8000);
      }
      else {
        var summary = data.content.REPORT.SUMMARY;
        var html = "<div>";
        html += "<div>";
        html += "<span>DATE : " + summary.DATE + "</span>";
        html += "<span>GEOGRAPHY : " + summary.GEOGRAPHY + "</span>";
        html += "<span>LOCATION : " + summary.LOCATION + "</span>";
        html += "<span>MEDIA : " + summary.MEDIA + "</span>";
        html += "<span>REACHOPTION : " + summary.REACHOPTION + "</span>";
        html += "<span>TARGET : " + summary.TARGET + "</span>";
        html += "<span>TIMEPERIOD : " + summary.TIMEPERIOD + "</span>";
        html += "<span>USER : " + summary.USER + "</span>";
        html += "</div>";
        html += "<table>";
        html += "<thead>";
        var cols = data.content.REPORT.TABLE.THEAD.TR.TD;
        html += "<tr>";
        for (var i = 0; i < cols.length; i++) {
          html += "<td>" + cols[i]._ + "</td>";
        }
        html += "</tr>";
        html += "</thead>";
        html += "<tbody>";
        var rows = data.content.REPORT.TABLE.TBODY.TR;
        for (var i = 0; i < rows.length; i++) {
          html += "<tr>";
          var cols = rows[i].TD;
          for (var j = 0; j < cols.length; j++) {
            html += "<td>" + cols[j]._ + "</td>";
          }
          html += "</tr>";
        }
        html += "</tbody>";
        html += "</table>";
        html += "</div>";
        $("#search-result").html(html);
      }
    });
  })(jQuery, Drupal, window);  
}
